package it.mikypro.armorstandlib.animations.types;

import it.mikypro.armorstandlib.ArmorStandLib;
import lombok.Getter;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;

public class Ascend extends BukkitRunnable {

    @Getter
    private ArmorStand stand;

    public Ascend(ArmorStand stand, long start_after, long stop_after) {
        this.stand = stand;

        BukkitRunnable animation = this;

        new BukkitRunnable() {
            @Override
            public void run() {
                animation.runTaskTimer(ArmorStandLib.getInstance(), 0, 1);
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), start_after);

        if (stop_after < 0) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                animation.cancel();
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), stop_after);
    }

    public void run() {

        getStand().teleport(stand.getLocation().clone().add(0, 0.05, 0));
    }
}
