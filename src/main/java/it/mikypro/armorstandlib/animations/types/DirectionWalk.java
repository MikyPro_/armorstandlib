package it.mikypro.armorstandlib.animations.types;

import it.mikypro.armorstandlib.ArmorStandLib;
import it.mikypro.armorstandlib.animations.Direction;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;

public class DirectionWalk extends BukkitRunnable {

    @Getter
    private Direction direction;

    @Getter
    private ArmorStand stand;

    public DirectionWalk(ArmorStand stand, Direction direction, long start_after, long stop_after) {
        this.stand = stand;
        this.direction = direction;

        BukkitRunnable animation = this;

        new BukkitRunnable() {
            @Override
            public void run() {
                animation.runTaskTimer(ArmorStandLib.getInstance(), 0, 1);
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), start_after);

        if (stop_after < 0) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                animation.cancel();
            }
        }.runTaskLaterAsynchronously(ArmorStandLib.getInstance(), stop_after);
    }

    public void run() {

        Location location = stand.getLocation();
        Direction directionFixed = direction;

        if (direction.equals(Direction.CARDINAL)) {

            directionFixed = Direction.getCardinalDirection(stand);
        }

        if (directionFixed == Direction.NORTH) {
            location.subtract(0, 0, 0.05); //180
        } else if (directionFixed == Direction.WEST) {
            location.subtract(0.05, 0, 0); //90
        } else if (directionFixed == Direction.SOUTH) {
            location.add(0, 0, 0.05); //0
        } else if (directionFixed == Direction.EAST) {
            location.add(0.05, 0, 0); //-90
        }

        getStand().teleport(location);
    }
}

