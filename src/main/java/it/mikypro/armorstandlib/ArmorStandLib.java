package it.mikypro.armorstandlib;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class ArmorStandLib extends JavaPlugin {

    public static Plugin getInstance() {

        return ArmorStandLib.getPlugin(ArmorStandLib.class);
    }
}